const Question = require('../models/questionModel')

exports.list_all_questions = (req, res) => {
  Question.find({}, (error, questions) => {
    error ? res.send(error) : res.send(questions)
  })
}

exports.create_a_question = (req, res) => {
  const new_question = new Question(req.body)
  console.log(new_question)
  new_question.save((error, question) => {
    error ? res.send(error) : res.send(question)
  })
}

exports.read_a_question = (req, res) => {
  Question.findById(req.params.questiId, (error, question) => {
    error ? res.send(error) : res.send(question)
  })
}

exports.update_a_question = (req, res) => {
  Question.finOneAndUpdate({_id: req.params.questId},
    req.body, {new: true}, (error, question) => {
      error ? res.send(error) : res.send(question)
  })
}

exports.delete_a_question = (req, res) => {
  Question.remove({
    _id: req.params.questionId
  }, (error, question) => {
      error ? res.send(error) : res.json({message: 'Question successfully deleted'})
  })
}
