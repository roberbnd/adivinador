const Character = require('../models/characterModel.js')

exports.list_all_characters = (req, res) => {
  Character.find({}, (error, characters) => {
    error ? res.send(error) : res.send(characters)
  })
}

exports.create_a_character = (req, res) => {
  const new_character = new Character(req.body)
  new_character.save((error, character) => {
    error ? res.send(error) : res.send(character)
  })
}

exports.read_a_character = (req, res) => {
  Character.findById(req.params.characterId, (error, character) => {
    error ? res.send(error) : res.send(character)
  })
}

exports.update_a_character = (req, res) => {
  Character.finOneAndUpdate({_id: req.params.characterId},
    req.body, {new: true}, (error, character) => {
      error ? res.send(error) : res.send(character)
  })
}

exports.delete_a_character = (req, res) => {
  Character.remove({
    _id: req.params.characterId
  }, (error, character) => {
      error ? res.send(error) : res.json({message: 'Character successfully deleted'})
  })
}
