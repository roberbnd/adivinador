const mongoose = require('mongoose')
const Schema = mongoose.Schema

const CharacterSchema = new Schema({
  name: {
    type: String
  },
  image: {
    type: String
  }
})

module.exports = mongoose.model('Character', CharacterSchema)
