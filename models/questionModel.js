const mongoose = require('mongoose')
const Schema = mongoose.Schema

const QuestionSchema = new Schema({
  id: {
    type: String,
    index: true
  },
  question: {
    type: String
  }
})

module.exports = mongoose.model('Question', QuestionSchema)
