import React from 'react';

import { Col, Row } from 'react-grid-system';

import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';

import Aux from '../../../hoc/aux'

const characters = props => {
  const styles = {
    card: {
      height: 250,
      width: 200
    },
    img: {
      height: 200,
      width: 200
    },
    title: {
      textAlign: 'center'
    }
  }

  return (
    <Aux>
      <Card style={styles.card} >
        <CardActionArea>
          <CardMedia
            image={require('../../../assets/characters/' + props.name + '.jpg')}
            style={styles.img} title="Goku" />
        </CardActionArea>
        <CardContent style={styles.title}>
          <Typography gutterBottom variant="headline" component="h2">
            { props.name.toUpperCase() }
          </Typography>
        </CardContent>
      </Card>
      <Button onClick={props.clicked}>Questions</Button>
      <Row>
        <Col disabled={props.showQuestions} sm={12}>
          { props.showQuestions ? props.children : null}
        </Col>
      </Row>
    </Aux>
  )
};

export default characters;
