import React, { Component } from 'react';
import axios from '../../hoc/axios'

import {  Row, Col } from 'react-grid-system';
import Button from '@material-ui/core/Button';
import Layout from '../../hoc/layout'

import Character from './character/character'
import UploadImage from '../UploadImage/UploadImage'

class characters extends Component {

  state = {
    selectedFile: ''
  }

  onChange = (e) => {
    this.setState({ selectedFile: e.target.files[0] });
    // switch (e.target.name) {
    //   case 'selectedFile':
    //     this.setState({ selectedFile: e.target.files[0] })
    //     break
    //     default:
        // this.setState({ [e.target.newCharacter.selectedFile]: e.target.value })
    // }
  }

  onSubmit = (e) => {
      e.preventDefault();
      const selectedFile = this.state.selectedFile;
      // tslint:disable-next-line
      console.log(selectedFile);
      
      let formData = new FormData();

      formData.append('selectedFile', selectedFile);

      axios.post('/characters', formData)
        .then((result) => {
          // access results...
          console.log(result)
        });
    // event.preventDefault();
    // // const { description, selectedFile } = this.state;
    // const selectedFile = this.state.selectedFile;
    // let formData = new FormData()
    //
    // // formData.append('description', description)
    // formData.append('selectedFile', selectedFile)
  // let axiosConfig = {
  //     headers: {
  //       'Content-Type': 'multipart/form-data',
  //     }
  //   };
    // fetch('http://localhost:3001/characters', {
    //   method: 'POST',
    //   body: formData
    // })
    // axios.post('/characters', formData, axiosConfig)
  // console.log(formData)
  //   axios.post('/characters/', formData)
      // .then(res => {
      //   console.log(res)
      // })
//  axios.post('http://localhost:3001/characters', formData, {
//     onUploadProgress: progressEvent => {
//       console.log(progressEvent.loaded / progressEvent.total)
//     }
//   })
    // axios({
    //   method: 'post',
    //   url: 'http://localhost:3001/characters',
    //   data: formData,
    //   config: { headers: {'Content-Type': 'multipart/form-data' }}
    // })
    //   .then(function (response) {
    //     //handle success
    //     // console.log(response);
    //   })
    //   .catch(function (response) {
    //     //handle error
    //     console.log(response);
    //   });
  }

  showQuestionHandler  = () => {
    // this.setState(prevState => {
    //   showQuestion: !prevState
    // })
    this.setState({showQuestions: !this.state.showQuestions})
  }

  render() {
    const names = ['16', '17', '18', '19', 'bills', 'wiss', 'raditz', 'tenshinhan',
      'milk', 'picoro', 'boo', 'bulma', 'goten', 'gohan', 'goku', 'roshi',
      'trunks', 'vegeta', 'videl', 'cell', 'popo'
    ]

    const characters = names.map(name  => {
      return (
        <Col sm={4}>
          <Character
            key={name}
            showQuestions={this.state.showQuestions}
            clicked={this.showQuestionHandler}
            name={name}>hello</Character>
        </Col>
      )
    })

    return (
      <Layout>
        <Row>
          <Col sm={12}>
            <form onSubmit={this.onSubmit} method="post" enctype="multipart/form-data">
              {/* <input */}
              {/*   type="text" */}
              {/*   name="description" */}
              {/*   value={this.state.description} */}
              {/*   onChange={this.onChange} */}
              {/* /> */}
              <input
                type="file"
                name="selectedFile"
                onChange={this.onChange}
              />
              <button type="submit">Submit</button>
            </form>
            {/* <Button variant="contained" color="secondary"> */}
            {/*   Add Characters */}
            {/* </Button> */}
            {/* <UploadImage /> */}
          </Col>
        </Row>
        <Row>
          {characters}
        </Row>
      </Layout>
    )
  }
}

export default characters;
