import React,{ Component } from 'react';
import axios from 'axios'

import Layout from '../../hoc/layout'
import TextField from '@material-ui/core/TextField';
import { Row, Col } from 'react-grid-system';

import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';

class Questions extends Component {
  constructor(props) {
    super(props)
    this.state = {
      question: '',
      questions: []
    }

    this.style = {
      input: {
        width: 100
      },
      question: {
        padding: 10 + 'px'
      }
    }
  }

  componentDidMount() {
    axios.get('http://localhost:3001/questions')
      .then(res => {
        return res.data
      } )
      .then(questions => {
        console.log(questions)
        this.setState({questions: questions})
      })
  }

  handleChange = event => {
    let question = event.target.value
    this.setState({question: question})
  };

  addQuestion = () => {
    let questions = this.state.questions
    let new_question = {
    question: this.state.question
  }
    questions.push(new_question)
    console.log(this.state.question)
    axios.post('http://localhost:3001/questions/', new_question)
      .then(res => {
        this.setState({questions: questions})
      })
  }

  deleteQuestionHandler  = (id) => {
    let questions = this.state.questions.filter( (q) => {
      return q._id !== id
    })
    console.log("deleting: " + id)
    axios.delete('http://localhost:3001/questions/' + id)
    .then(res => {
      console.log("deleted")
    this.setState({questions: questions})
    })
  }

  render() {
    let questions = []
    if (this.state.questions) {
      questions = this.state.questions.map(( question, index ) => {
        return (
          <Col key={index} sm={12}>
            <div style={this.style.question}>
              <Card onClick={() => this.deleteQuestionHandler(question._id)}>
                <CardContent>
                  <Typography variant="headline" component="h2">
                    {question.question}
                  </Typography>
                </CardContent>
              </Card>
            </div>
          </Col>
        )
      })
    }

    // console.log(this.props)
    return (
      <Layout>
        <Row>
          <Col sm={12}>
            <TextField
              style={{width:'100%'}}
              id="question"
              onChange={this.handleChange}
              label="Question"
              value={this.state.question}
              margin="normal"
            />
          <Button onClick={this.addQuestion} variant="contained" color="secondary">
            Add Question
          </Button>
        </Col>
        </Row>
        <Row>
          {questions}
        </Row>
      </Layout>
    )
  }

};

export default Questions;
