import React, { Component } from 'react'
import { Row, Col } from 'react-grid-system';
import { Link } from 'react-router-dom'

import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';

import './Header.css'
class Header extends Component {

  goHome = (props) => {
    this.history({
      pathname: '/hello'
    })
    // this.history.replace('/')
  }

  deleteDB = () => {
    // delete personajes y perguntas
    console.log("this deleteDB")
    // axios.delete()
  }

  render() {
    return (
      <Row>
        <Col className="padding" sm={12}>
          <header className="App-header">
            <AppBar position="static">
              <Toolbar>
                <Typography variant="title" color="inherit">
                  Sistema Experto
                </Typography>
                <Link className='menu' to='/'>
                  <Button color="inherit">Home</Button>
                </Link>
                <Link className='menu' to='/play'>
                  <Button color="inherit">Play</Button>
                </Link>
                <Link className='menu' to='/characters'>
                  <Button color="inherit">Characters</Button>
                </Link>
                <Link className='menu' to='/questions'>
                  <Button color="inherit">Questions</Button>
                </Link>
                <Button color="inherit" onClick={this.deleteDB}>Delete DB</Button>
              </Toolbar>
            </AppBar>
          </header>
        </Col>
      </Row>
    )
  }
}

export default Header;
