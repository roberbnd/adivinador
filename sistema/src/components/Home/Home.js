import React, { Component } from 'react'
import Layout from '../../hoc/layout'
import { Row, Col } from 'react-grid-system';
import { Link } from 'react-router-dom'

import './Home.css'
import Genio from '../../assets/genio.png'

import Typography from '@material-ui/core/Typography';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Button from '@material-ui/core/Button';

class Home extends Component {

  render() {
    return (
      <Layout>
        <Row>
          <Col sm={6}>
            <img alt="genio" className="genio" src={Genio} />
          </Col>
          <Col sm={6}>
            <Card>
              <CardContent>
                <Typography variant="headline">
                  I can guess you favorite character of Dragon Ball Super,
                  Let's go!!!
                </Typography>
              </CardContent>
            </Card>
            <div className="startButton">
              <Link to='/play'>
                <Button variant="contained" color="primary" size="large">
                  Start
                </Button>
              </Link>
            </div>
          </Col>
        </Row>
      </Layout>
    )
  }
};

export default Home;
