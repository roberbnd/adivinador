import axios from 'axios'

const instance = axios.create({
  // baseURL: 'https://minisistema-b78fd.firebaseio.com/ '
  baseURL: 'http://localhost:3001'
})

export default instance;
