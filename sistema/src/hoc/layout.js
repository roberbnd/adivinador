import React from 'react';
import Header from '../components/Header/Header'
import { Container, Row, Col } from 'react-grid-system';

const layout = props => (
  <Container>
    <Row>
      <Col>
        <Header />
      </Col>
    </Row>
    {props.children}
  </Container>
);

export default layout;
