import React, { Component } from 'react';
import { Route, Switch } from 'react-router-dom'

import Home from './components/Home/Home'
import Play from './components/Play/Play'
import Questions from './components/Question/Question'
import Characters from './components/Characters/Characters'

class App extends Component {
  render() {
    return (
      <div>
        <Switch>
          <Route path="/play" exact component={Play}/>
          <Route path="/questions" exact component={Questions}/>
          <Route path="/characters" exact component={Characters}/>
          <Route path="/" exact component={Home}/>
        </Switch>
      </div>
    );
  }
}

export default App;
