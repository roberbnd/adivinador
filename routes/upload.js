const express = require('express');
const router = express.Router();
const UploadImage = require('../controllers/photosController.js')

router.post('/', UploadImage.upload)

module.exports = router;
