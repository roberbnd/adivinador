const express = require('express');
const router = express.Router();
const Characters = require('../controllers/characterController.js')

const multer = require('multer');
const uuidv4 = require('uuid/v4');
const path = require('path');

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    /**
    |--------------------------------------------------
    | files will be saved in the 'uploads' directory. Make
    | sure this directory already exists!
    |--------------------------------------------------
    */
    cb(null, './uploads')
  },
  filename: (req, file, cb) => {
    /**
    |--------------------------------------------------
    | it will be available as req.file.pathname in the
    | router handler
    |--------------------------------------------------
    */
    const newFilename = `${uuidv4()}${path.extname(file.originalname)}`
    cb(null, newFilename)
  }
})
// create the multer instance that will be used to upload/save the file
const upload = multer({ storage })

router.get('/', Characters.list_all_characters)
router.post('/', upload.single('selectedFile'), (req, res) => {
  console.log(req.file);
  // console.log(req.file.pathname)
  res.send()
});
// router.post('/', Characters.create_a_character)
router.get('/:characterId', Characters.read_a_character)
router.put('/:characterId', Characters.update_a_character)
router.delete('/:characterId', Characters.delete_a_character)


module.exports = router;
