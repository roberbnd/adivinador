const express = require('express');
const router = express.Router();
const Question = require('../controllers/questionController.js')

// module.exports = (app) => {
//   const Question = require('../controllers/questionController.js')
//
//   app.route(/)
//     .get('/', Question.list_all_questions)
//     .post('/', Question.create_a_question)
// }

router.get('/', Question.list_all_questions)
router.post('/', Question.create_a_question)
router.get('/:questionId', Question.read_a_question)
router.put('/:questionId', Question.update_a_question)
router.delete('/:questionId', Question.delete_a_question)

module.exports = router;
